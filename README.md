auditd
=========

The auditd subsystem is an access monitoring and accounting for Linux developed and maintained by RedHat. It was designed to integrate pretty tightly with the kernel and watch for interesting system calls

Requirements
------------

None

Role Variables
--------------

enable_auditd: true - [parameter to enable/diasble module]

auditd_package_name: "audit" - [name of the package and service]

auditd_service_enable: yes - [whether to enable the service]

auditd_service_ensure: "started" - [state of the service]

auditd_service_restart: - [command to restart the service]

auditd_rules_file: "/etc/audit/rules.d/ansible.rules" - [name of the rules file ]

auditd_params: - [auditd config parameters]

auditd_rules: - [list of rules to be configured in the rules file]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
