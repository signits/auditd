---
# defaults file for auditd
enable_auditd: true
auditd_package_name: "audit"
auditd_service_enable: yes
auditd_service_ensure: "started"
auditd_service_restart: "/usr/libexec/initscripts/legacy-actions/auditd/restart"
auditd_rules_file: "/etc/audit/rules.d/ansible.rules"

auditd_params:
  # Main config file variables
  log_file: '/var/log/audit/audit.log'
  log_format: 'RAW'
  log_group: 'root'
  write_logs: false
  priority_boost: 4
  flush: 'incremental'
  freq: 20
  num_logs: 5
  disp_qos: 'lossy'
  dispatcher: '/sbin/audispd'
  name_format: 'none'
  admin: "{{ ansible_hostname }}"
  max_log_file: 6
  max_log_file_action: 'rotate'
  space_left: 75
  space_left_action: 'syslog'
  action_mail_acct: 'root'
  admin_space_left: 50
  admin_space_left_action: 'suspend'
  disk_full_action: 'suspend'
  disk_error_action: 'suspend'
  tcp_listen_port: false
  tcp_listen_queue: 5
  tcp_max_per_addr: 1
  tcp_client_ports: false
  tcp_client_max_idle: 0
  enable_krb5: 'no'
  krb5_principal: 'auditd'
  krb5_key_file: false
  # Rules Header variables
  buffer_size: 8192
  # Audisp main config variables
  audisp_q_depth: 80
  audisp_overflow_action: 'syslog'
  audisp_priority_boost: 4
  audisp_max_restarts: 10
  audisp_name_format: 'none'
  audisp_name: false
  # Give the option of managing the service.
  manage_service: true

auditd_rules:
  'watch for changes where the system date and/or time has been modified - 1':
    content: '-a always,exit -F arch=b64 -S adjtimex -S settimeofday -k time-change'
    order: 1
  'watch for changes where the system date and/or time has been modified - 2':
    content: '-a always,exit -F arch=b32 -S adjtimex -S settimeofday -S stime -k time-change'
    order: 2
  'watch for changes where the system date and/or time has been modified - 3':
    content: '-a always,exit -F arch=b64 -S clock_settime -k time-change'
    order: 3
  'watch for changes where the system date and/or time has been modified - 4':
    content: '-a always,exit -F arch=b32 -S clock_settime -k time-change'
    order: 4
  'watch for changes where the system date and/or time has been modified - 5':
    content: '-w /etc/localtime -p wa -k time-change'
    order: 5
  'watch for changes where user/group information is changed - 1':
    content: '-w /etc/group -p wa -k identity'
    order: 6
  'watch for changes where user/group information is changed - 2':
    content: '-w /etc/passwd -p wa -k identity'
    order: 7
  'watch for changes where user/group information is changed - 3':
    content: '-w /etc/gshadow -p wa -k identity'
    order: 8
  'watch for changes where user/group information is changed - 4':
    content: '-w /etc/shadow -p wa -k identity'
    order: 9
  'watch for changes where user/group information is changed - 5':
    content: '-w /etc/security/opasswd -p wa -k identity'
    order: 10
  '5.2.6 - Record Events That Modify the Systems Network Environment - 1':
    content: '-a always,exit -F arch=b64 -S sethostname -S setdomainname -k system-locale'
    order: 11
  '5.2.6 - Record Events That Modify the Systems Network Environment - 2':
    content: '-a always,exit -F arch=b32 -S sethostname -S setdomainname -k system-locale'
    order: 12
  '5.2.6 - Record Events That Modify the Systems Network Environment - 3':
    content: '-w /etc/issue -p wa -k system-locale'
    order: 13
  '5.2.6 - Record Events That Modify the Systems Network Environment - 4':
    content: '-w /etc/issue.net -p wa -k system-locale'
    order: 14
  '5.2.6 - Record Events That Modify the Systems Network Environment - 5':
    content: '-w /etc/hosts -p wa -k system-locale'
    order: 15
  '5.2.6 - Record Events That Modify the Systems Network Environment - 6':
    content: '-w /etc/sysconfig/network -p wa -k system-locale'
    order: 16
  '5.2.7 - Record Events That Modify the Systems Mandatory Access Controls - 1':
    content: '-w /etc/selinux/ -p wa -k MAC-policy'
    order: 17
  '5.2.8 - Collect Login and Logout Events - 1':
    content: '-w /var/log/faillog -p wa -k logins'
    order: 18
  '5.2.8 - Collect Login and Logout Events - 2':
    content: '-w /var/log/lastlog -p wa -k logins'
    order: 19
  '5.2.8 Collect Login and Logout Events - 3':
    content: '-w /var/log/tallylog -p wa -k logins'
    order: 20
  '5.2.9 Collect Session Initiation Information - 1':
    content: '-w /var/run/utmp -p wa -k session'
    order: 21
  '5.2.9 Collect Session Initiation Information - 2':
    content: '-w /var/run/wtmp -p wa -k session'
    order: 22
  '5.2.9 Collect Session Initiation Information - 3':
    content: '-w /var/run/btmp -p wa -k session'
    order: 23
  '5.2.10 Collect Session Initiation Information - 1':
    content: '-a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod'
    order: 24
  '5.2.10 Collect Session Initiation Information - 2':
    content: '-a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod'
    order: 25
  '5.2.10 Collect Session Initiation Information - 3':
    content: '-a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod'
    order: 26
  '5.2.10 Collect Session Initiation Information - 4':
    content: '-a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod'
    order: 27
  '5.2.10 Collect Session Initiation Information - 5':
    content: '-a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod'
    order: 28
  '5.2.10 Collect Session Initiation Information - 6':
    content: '-a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod'
    order: 29
  '5.2.11 Collect Unsuccessful Unauthorized Access Attempts to Files - 1':
    content: '-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access'
    order: 30
  '5.2.11 Collect Unsuccessful Unauthorized Access Attempts to Files - 2':
    content: '-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access'
    order: 31
  '5.2.11 Collect Unsuccessful Unauthorized Access Attempts to Files - 3':
    content: '-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access'
    order: 32
  '5.2.11 Collect Unsuccessful Unauthorized Access Attempts to Files - 5':
    content: '-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access'
    order: 33
  '5.2.13 Collect Successful File System Mounts - 1':
    content: '-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts'
    order: 34
  '5.2.13 Collect Successful File System Mounts - 2':
    content: '-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts'
    order: 35
  '5.2.14 Collect File Deletion Events by User - 1':
    content: '-a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete'
    order: 36
  '5.2.14 Collect File Deletion Events by User - 2':
    content: '-a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete'
    order: 37
  '5.2.15 Collect Changes to System Administration Scope - 1':
    content: '-w /etc/sudoers -p wa -k scope'
    order: 38
  '5.2.16 Collect System Administrator Actions - 1':
    content: '-w /var/log/sudo.log -p wa -k actions'
    order: 39
  '5.2.17 Collect Kernel Module Loading and Unloading - 1':
    content: '-w /sbin/insmod -p x -k modules'
    order: 40
  '5.2.17 Collect Kernel Module Loading and Unloading - 2':
    content: '-w /sbin/rmmod -p x -k modules'
    order: 41
  '5.2.17 Collect Kernel Module Loading and Unloading - 3':
    content: '-w /sbin/modprobe -p x -k modules'
    order: 42
  '5.2.17 Collect Kernel Module Loading and Unloading - 4':
    content: '-a always,exit -F arch=b64 -S init_module -S delete_module -k modules'
    order: 43
  '5.2.18 Make the Audit Configuration Immutable':
    content: '-e 2'
    order: 44